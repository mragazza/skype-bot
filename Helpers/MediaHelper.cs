using System;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Extensions.Logging;

namespace SkypeBot.Helpers
{
    public class MediaHelper
    {
        private readonly IWebHostEnvironment _environment;
        private readonly ILogger<BotFrameworkHttpAdapter> _logger;

        public MediaHelper(IWebHostEnvironment environment, ILogger<BotFrameworkHttpAdapter> logger)
        {
            _environment = environment;
            _logger = logger;
        }

        public string GetFilePath(string fileName)
        {
            return Path.Combine(_environment.WebRootPath, "media", fileName);
        }

        public string GetFileData(string filePath)
        {
            string data = "";
            try
            {
                if (File.Exists(filePath))
                {
                    data = Convert.ToBase64String(File.ReadAllBytes(filePath));
                }

            }
            catch (System.Exception ex)
            {
                _logger.LogError(ex, $"[OnTurnError] unhandled error : {ex.Message}");
            }

            return data;
        }

        public string GetImageData(string imagePath)
        {
            string data = GetFileData(imagePath);
            string extension = Path.GetExtension(imagePath) ?? "jpg";

            return $"data:image/{extension};base64,{data}";
        }
    }
}