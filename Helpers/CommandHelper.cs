using System;
using Microsoft.Bot.Connector;

namespace SkypeBot.Helpers
{

    public class CommandHelper
    {
        public const string START = "start";
        public const string HELP = "help";
        public const string ABOUT = "about";

        public static string GetSeparator(string channel)
        {
            string separator = "";
            switch (channel)
            {
                case Channels.Skype:
                case Channels.Skypeforbusiness:
                    separator = "\\";
                    break;
                case Channels.Telegram:
                    separator = "/";
                    break;
                default:
                    separator = "/";
                    break;
            }
            return separator;
        }

        public static string GetCommand(string channel, string command)
        {
            return String.Concat(CommandHelper.GetSeparator(channel), command);
        }

        public static string GetRawCommand(string channel, string command)
        {
            string separator = CommandHelper.GetSeparator(channel);
            int index = command.IndexOf(separator, StringComparison.Ordinal);
            return (index == 0) ? command.Remove(0, separator.Length) : command;
        }
    }
}