﻿using Microsoft.Bot.Builder;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SkypeBot.Middleware
{
    public class BotLocaleMiddleware : IMiddleware
    {
        private readonly string _defaultLocale;

        public static readonly string[] availableCultures = { "ru", "en" };

        public BotLocaleMiddleware(string defaultLocale)
        {
            _defaultLocale = defaultLocale;
        }

        public async Task OnTurnAsync(ITurnContext turnContext, NextDelegate next, CancellationToken cancellationToken = default)
        {
            var cultureInfo = string.IsNullOrEmpty(turnContext.Activity.Locale)
                   ? new CultureInfo(_defaultLocale)
                   : new CultureInfo(turnContext.Activity.Locale);

            if(!availableCultures.Contains(cultureInfo.TwoLetterISOLanguageName))
            {
                cultureInfo = new CultureInfo(_defaultLocale);
            }

            CultureInfo.CurrentUICulture = CultureInfo.CurrentCulture = cultureInfo;

            await next(cancellationToken).ConfigureAwait(false);
        }
    }
}
