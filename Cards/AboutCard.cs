﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Localization;
using SkypeBot.Helpers;
using System.Collections.Generic;

namespace SkypeBot.Cards
{
    public static class AboutCard
    {
        public static AnimationCard Get(ITurnContext turnContext, IStringLocalizer<SkypeBot> localizer)
        {
            var card = new AnimationCard
            {
                Title = localizer["AboutCardTitle"],
                Text = localizer["AboutCardText"],
                Media = new List<MediaUrl>
                {
                    new MediaUrl()
                    {
                        Url = "https://pa1.narvii.com/5829/3fb4c1feefcd2383f03e214c700544e31142bfa1_hq.gif",
                    },
                },
                Buttons = new List<CardAction>()
                {
                    new CardAction(ActionTypes.PostBack, localizer["Return"], null, localizer["Return"], localizer["Return"], CommandHelper.GetCommand(turnContext.Activity.ChannelId, "help"), null)
                }
            };

            return card;
        }
    }
}
