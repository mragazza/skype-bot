﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using SkypeBot.Helpers;
using System.Collections.Generic;

namespace SkypeBot.Cards
{
    public static class HelpCard
    {
        public static ThumbnailCard Get(ITurnContext turnContext, IStringLocalizer<SkypeBot> localizer, IConfiguration configuration)
        {
            var card = new ThumbnailCard
            {
                Title = localizer["HelpCardTitle"],
                Text = localizer["HelpCardText"],
                Buttons = new List<CardAction>()
                {
                    new CardAction(ActionTypes.PostBack, localizer["AboutBot"], null, localizer["AboutBot"], localizer["AboutBot"], CommandHelper.GetCommand(turnContext.Activity.ChannelId, "about"), null),
                    new CardAction(ActionTypes.OpenUrl, localizer["WriteToMaintainer"], null, localizer["WriteToMaintainer"], localizer["WriteToMaintainer"], string.Concat("mailto:", configuration.GetValue<string>("supportEmail")), null)
                }
            };

            return card;
        }
    }
}
