﻿using AdaptiveCards;
using AdaptiveCards.Templating;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using SkypeBot.Helpers;
using System.Collections.Generic;
using System.IO;

namespace SkypeBot.Cards
{
    public static class LangCard
    {
        public static object Get(ITurnContext turnContext, IStringLocalizer<SkypeBot> localizer, IConfiguration configuration, MediaHelper mediaHelper)
        {
            string imagePath = mediaHelper.GetFilePath("hero.jpg") ?? "";
            string imageData = mediaHelper.GetImageData(imagePath);

            string templatepath = Path.GetFullPath(Path.Combine(".", "Cards", "Templates", "LangCard.json"));
            AdaptiveCardTemplate template = new AdaptiveCardTemplate(JsonConvert.DeserializeObject(File.ReadAllText(templatepath)));
            var cardData = new
            {
                Header = "Новости Кью-бота",
                TextUnderHeader = "Горячие новости на сегодня:",
                LeftColumnImageUrl = "https://i.redd.it/3283tvxdn0r31.jpg",
                HeaderRightColumn = "«Мама, я в телевизоре!»",
                TextRightColumn = "Кью попал на митап по ботам в ЭФКО. Bot Framework SDK, карточки и эмулятор ждут.",
                HeaderSubCard = "Оставить отзыв!",
                CommentPlaceholder = "Напишите свой комментарий",
                HeaderMaintainerPage = "Страничка разработчика",
                MaintainerUrl = "https://gitlab.com/mragazza"
            };
            string card = template.Expand(cardData);

            return JsonConvert.DeserializeObject(card);
        }
    }
}
