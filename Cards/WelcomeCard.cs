﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using SkypeBot.Helpers;
using System.Collections.Generic;
namespace SkypeBot.Cards
{
    public static class WelcomeCard
    {
        public static HeroCard Get(ITurnContext turnContext, IStringLocalizer<SkypeBot> localizer, IConfiguration configuration, MediaHelper mediaHelper)
        {
            string imagePath = mediaHelper.GetFilePath("hero.jpg") ?? "";
            string imageData = mediaHelper.GetImageData(imagePath);
            var card = new HeroCard
            {
                Title = localizer["IntoTitle"],
                Text = localizer["IntoText"],
                Images = new List<CardImage>() { new CardImage("https://upload.wikimedia.org/wikipedia/en/6/60/KyubeyMadokaMagica.png") },
                Buttons = new List<CardAction>()
                {
                    new CardAction(ActionTypes.PostBack, localizer["ToHelp"], null, localizer["ToHelp"], localizer["ToHelp"], CommandHelper.GetCommand(turnContext.Activity.ChannelId, "help"), null),
                    new CardAction(ActionTypes.PostBack, localizer["AboutBot"], null, localizer["AboutBot"], localizer["AboutBot"], CommandHelper.GetCommand(turnContext.Activity.ChannelId, "about"), null),
                    new CardAction(ActionTypes.OpenUrl, localizer["WriteToMaintainer"], null, localizer["WriteToMaintainer"], localizer["WriteToMaintainer"], string.Concat("mailto:",  configuration.GetValue<string>("supportEmail")), null)
                }
            };

            return card;
        }
    }
}
