using Microsoft.AspNetCore.Hosting;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using SkypeBot.Cards;
using SkypeBot.Helpers;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace SkypeBot
{
    public class SkypeBot : ActivityHandler
    {
        private readonly IStringLocalizer<SkypeBot> _localizer;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IConfiguration _configuration;
        private readonly MediaHelper _mediaHelper;

        public SkypeBot(IStringLocalizer<SkypeBot> localizer, IWebHostEnvironment hostEnvironment, IConfiguration configuration, MediaHelper mediaHelper)
        {
            _localizer = localizer;
            _hostEnvironment = hostEnvironment;
            _configuration = configuration;
            _mediaHelper = mediaHelper;
        }

        protected override async Task OnMembersAddedAsync(IList<ChannelAccount> membersAdded, ITurnContext<IConversationUpdateActivity> turnContext, CancellationToken cancellationToken)
        {
            if(!(turnContext.Activity.Conversation.IsGroup ?? false))
            {
                foreach (var member in membersAdded)
                {
                    if (member.Id != turnContext.Activity.Recipient.Id)
                    {
                        await ShowWelcomeAsync(turnContext, cancellationToken);
                    }
                }
            } else
            {
                await turnContext.SendActivityAsync(MessageFactory.Text(_localizer["WelcomeTextInGroup"]), cancellationToken);
            }
        }

        protected override async Task OnMessageActivityAsync(ITurnContext<IMessageActivity> turnContext, CancellationToken cancellationToken)
        {
            var text = CommandHelper.GetRawCommand(turnContext.Activity.ChannelId, turnContext.Activity.Text.ToLowerInvariant());
            switch (text)
            {
                case "start":
                    await ShowWelcomeAsync(turnContext, cancellationToken);
                    break;
                case "help":
                    await ShowHelpAsync(turnContext, cancellationToken);
                    break;
                case "about":
                    await ShowAboutAsync(turnContext, cancellationToken);
                    break;
                case "test":
                    await ShowTestAsync(turnContext, cancellationToken);
                    break;
                default:
                    await turnContext.SendActivityAsync("Test", cancellationToken: cancellationToken);
                    break;
            }
        }

        private async Task ShowWelcomeAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            var card = WelcomeCard.Get(turnContext, _localizer, _configuration, _mediaHelper);
            var response = MessageFactory.Attachment(card.ToAttachment());
            await turnContext.SendActivityAsync(response, cancellationToken);
        }

        private async Task ShowHelpAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            var card = HelpCard.Get(turnContext, _localizer, _configuration);
            var response = MessageFactory.Attachment(card.ToAttachment());
            await turnContext.SendActivityAsync(response, cancellationToken);
        }

        private async Task ShowAboutAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            var card = AboutCard.Get(turnContext, _localizer);
            var response = MessageFactory.Attachment(card.ToAttachment());
            await turnContext.SendActivityAsync(response, cancellationToken);
        }

        private async Task ShowTestAsync(ITurnContext turnContext, CancellationToken cancellationToken)
        {
            var card = LangCard.Get(turnContext, _localizer, _configuration, _mediaHelper);
            var response = MessageFactory.Attachment(new Attachment 
            { 
                ContentType = "application/vnd.microsoft.card.adaptive",
                Content = card
            });
            await turnContext.SendActivityAsync(response, cancellationToken);
        }
    }
}
