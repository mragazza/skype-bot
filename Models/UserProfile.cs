﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SkypeBot.Models
{
    public class UserProfile
    {
        public string Locale { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string Name { get; set; }
    }
}
