﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Integration.AspNet.Core;
using Microsoft.Bot.Builder.TraceExtensions;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using SkypeBot.Middleware;

namespace SkypeBot
{
    public class AdapterWithErrorHandler : BotFrameworkHttpAdapter
    {
        public AdapterWithErrorHandler(IConfiguration configuration, ILogger<BotFrameworkHttpAdapter> logger, IStringLocalizer<SkypeBot> localizer)
            : base(configuration, logger)
        {
            string defaultLocale = configuration.GetValue<string>("defaultLocale") ?? "ru-RU";
            Use(new BotLocaleMiddleware(defaultLocale));

            OnTurnError = async (turnContext, exception) =>
            {
                logger.LogError(exception, $"[OnTurnError] unhandled error : {exception.Message}");

                await turnContext.SendActivityAsync(localizer["GlobalErrorTitle"]);
                await turnContext.SendActivityAsync(string.Concat(localizer["GlobalErrorText"], "\r\n", configuration.GetValue<string>("supportEmail")));

                await turnContext.TraceActivityAsync("OnTurnError Trace", exception.Message, "https://www.botframework.com/schemas/error", "TurnError");
            };
        }
    }
}
